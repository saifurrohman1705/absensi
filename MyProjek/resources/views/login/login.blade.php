@extends('login.layout')

@section('title')
    Login
@endsection

@section('content')
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="login100-form validate-form" method="post" action="/cek_login">
					{{csrf_field()}}
					<span class="login100-form-title p-b-49">
						Login
					</span>
					@if ($errors->any())
		                <div class="alert alert-danger alert-message">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		            @endif
					@if(\Session::has('alert'))
		                <div class="alert alert-danger alert-message">
		                    <div>{{Session::get('alert')}}</div>
		                </div>
            		@endif
            		@if(\Session::has('alert-sukses'))
		                <div class="alert alert-success alert-message">
		                    <div>{{Session::get('alert-sukses')}}</div>
		                </div>
            		@endif
					<div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Enter your username">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="Enter your password">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					
					<div class="text-right p-t-8 p-b-31">
						<a href="#">
							Forgot password?
						</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Login
							</button>
						</div>
					</div>

				</form>
			</div>

			<div class="col-md-12" style="margin-top: 10px;">
				<center>
					<div class="wrap-login100" style="padding: 20px">
						<h4>Default Password: datakreatif</h4>
					</div>
				</center>
			</div>
@endsection