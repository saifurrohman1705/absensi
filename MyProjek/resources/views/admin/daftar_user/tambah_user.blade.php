
@extends('admin.layout')

@section('title')
    Pilih Pegawai
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Pilih Pegawai</h1>
	    </div>
    </div>
    <div class="row">
    			<div class="col-md-12" style="margin-bottom:10px">
                    <a href="#" class="btn btn-danger" onclick="window.history.go(-1)"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <div class="col-lg-12">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b>Tabel Pegawai</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	
                        	
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width:20px">#</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th class="text-center">Aktif</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($pegawai))
									@foreach($pegawai as $data)
                                    <tr class="odd gradeX">
                                        <td>{{$no++}}</td>
                                        <td>{{$data->nik}}</td>
                                        <td>{{$data->nama}}</td>
                                        <td>{{$data->nama_jabatan}}</td>
                                        <td class="text-center">{{$data->aktif}}</td>
                                        <td class="text-center">
                                            <form method="POST" action="/p_tambah_user">
                                            {{csrf_field()}}
                                                <input type="hidden" name="level" value="{{$id}}">
                                                <input type="hidden" name="nama" value="{{$data->nama}}">
                                                <input type="hidden" name="username" value="{{$data->nik}}">
                                                <input type="hidden" name="id_pegawai" value="{{$data->id}}">
                                        	   <button type="submit" class="btn btn-success btn-sm" onclick="return confirm('Yakin mau nambah {{ $data->nama }} sebagai {{$nama}} ?')"><i class="fa fa-plus"></i> Tambah</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
								@else
									<tr>
										<td colspan="5"><h1 align="center">Data Tidak Ditemukan</h1></td>				
										</td>
									</tr>
                    			@endif
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@endsection
