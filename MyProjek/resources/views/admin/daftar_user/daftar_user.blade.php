
@extends('admin.layout')

@section('title')
    Daftar Divisi
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Daftar Users</h1>
	    </div>
    </div>
    <div class="row">
    			<div class="col-md-12" style="margin-bottom:10px">
                    <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalTambah"><i class="fa fa-plus"></i> Tambah</button>
                    <!-- Modal -->
                    <div id="ModalTambah" class="modal fade" role="dialog">
                      <div class="modal-dialog ">

                        <!-- Modal content-->
                        <div class="modal-content">
                        <form action="p_tambah_divisi" method="POST">
                        {{csrf_field()}}
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center">Silahkan Pilih Level</h4>
                          </div>
                          
                          <div class="modal-body text-center">
                            <a href="{{ url('/') }}\1\tambah_user" class="btn btn-success btn-lg">Operator</a>
                            <a href="{{ url('/') }}\2\tambah_user" class="btn btn-warning btn-lg">User</a> 
                          </div>
                          </form>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="col-lg-12">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b>Tabel Users</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">	
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width:20px">#</th>
                                        <th>Nama Users</th>
                                        <th>Username</th>
                                        <th>Level</th>
                                        <th>Aktif</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($user))
									             @foreach($user as $data)
                                    <tr class="odd gradeX">
                                        <td>{{$no++}}</td>
                                        <td>{{$data->nama}}</td>
                                        <td>{{$data->username}}</td>
                                        <td align="center">
                                          @if($data->level==0)
                                            Admin
                                          @elseif($data->level==1)
                                            <button type="button" class=" btn btn-warning btn-sm" data-toggle="modal" data-target="#ModalEdit{{$data->id}}"></i> Operator</button>
                                          @else
                                             <button type="button" class=" btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalEdit{{$data->id}}"></i> User</button>
                                          @endif
                                        </td>
                                        <td class="text-center">
                                          <form action="/{{$data->id}}/ubah_aktif_user" method="POST">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="PUT">
                                            @if($data->aktif=="Y")
                                                <input type="submit" class="btn btn-success btn-sm" onclick="return confirm('Yakin Mau Mengubah {{$data->nama}} Menjadi Tidak Aktif?')" value="Aktif" name="status">
                                            @else
                                               <input type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Mengubah {{$data->nama}} Menjadi Aktif?')" value="Tidak Aktif" name="status">
                                            @endif
                                          </form>
                                        </td>
                                        <td class="text-center">
                                            <form method="POST" action="/{{$data->id}}/delete_users">
                                            {{csrf_field()}} 
                                        	   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau hapus data {{ $data->nama }}?')" name="delete"><i class="fa fa-eraser"></i> Hapus</button>
                                               <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                        </td>
                                    </tr>
                                    
                                    {{-- Modal Edit --}}
                                    <div id="ModalEdit{{$data->id}}" class="modal fade" role="dialog">
                                          <div class="modal-dialog modal-md">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                            <form action="/{{$data->id}}/p_edit_user" method="POST">
                                            {{csrf_field()}}
                                             <input type="hidden" name="_method" value="PUT">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Edit User <span style="font-size:12px" class="label label-success">{{$data->nama}}</span></h4>
                                              </div>
                                              <div class="modal-body">
                                                <label class="control-label">Hari:</label>
                                                <select name="hari" class="form-control" required>

                                                <label class="control-label">Jadwal:</label>
                                                <input type="text" class="form-control" name="jadwal" value="{{$data->jadwal}}" placeholder="exp: regular, libur, meeting" required>

                                                <label class="control-label">Jam Masuk:</label>
                                                <input type="time" class="form-control" name="masuk" value="{{$data->masuk}}" >

                                                <label class="control-label">Jam Keluar:</label>
                                                <input type="time" class="form-control" name="keluar" value="{{$data->keluar}}" >
                                              </div>
                                              <div class="modal-footer">
                                                <input type="submit" class="btn btn-primary" value="Simpan"> 
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                              </form>
                                            </div>

                                          </div>
                                    </div>
                                    @endforeach
								@else
									<tr>
										<td colspan="5"><h1 align="center">Data Tidak Ditemukan</h1></td>				
										</td>
									</tr>
                    			@endif
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection
