
@extends('admin.layout')

@section('title')
    Detail Pegawai
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Detail Pegawai</h1>
	    </div>
    </div>
    <div class="row">
    	<div class="col-md-12" style="margin-bottom:10px">
             <a href="#" class="btn btn-danger" onclick="window.history.go(-1)"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
            <form action="/{{$data->id}}/p_edit" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-md-8">
                <table class="table table-stripped">
                    <tr>
                        <td style="width:30%"><label>NIK</label><label style="float:right">:</label></td>
                        <td>{{$data->nik}} </td>
                    </tr>
                    <tr>
                        <td><label>Nama Pegawai</label><label style="float:right">:</label></td>
                        <td>{{$data->nama}}</td>
                    </tr>
                    <tr>
                        <td><label>Jenis Kelamin</label><label style="float:right">:</label></td>
                        <td>
                            @if($data->jenis_kelamin=="L")
                                Laki-Laki 
                            @else
                                Perempuan
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><label>Jabatan Pegawai</label><label style="float:right">:</label></td>
                        <td>{{$data->nama_jabatan}}</td>
                    </tr>
                    <tr>
                        <td><label> Divisi</label><label style="float:right">:</label></td>
                        <td>
                            {{$data->nama_divisi}}
                        </td>
                    </tr>
                    <tr>
                        <td><label> Aktif </label><label style="float:right">:</label></td>
                        <td>
                            @if($data->aktif=="Y")
                                Ya 
                            @else
                                Tidak
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
             </div> 
             <div class="col-md-4">
            <div class="control-group">
                <label class="control-label">Foto </label><br/>
                <img src="{{ asset('image/'.$data->gambar) }}" id="showgambar" style="width:150px;margin:5px" />
                <input type="hidden" value="{{$data->gambar}}" name="nama_gambar">
            </div>
             </div> 
            </form>
        
                <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@endsection
