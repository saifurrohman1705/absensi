
@extends('admin.layout')

@section('title')
    Daftar Pegawai
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Daftar Pegawai</h1>
	    </div>
    </div>
    <div class="row">
    			<div class="col-md-12" style="margin-bottom:10px">
                    <a href="{{ url('/tambah_pegawai') }}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                <div class="col-lg-12">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b>Tabel Pegawai</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	
                        	
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width:20px">#</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th class="text-center">Aktif</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($pegawai))
									@foreach($pegawai as $data)
                                    <tr class="odd gradeX">
                                        <td>{{$no++}}</td>
                                        <td>{{$data->nik}}</td>
                                        <td>{{$data->nama}}</td>
                                        <td>{{$data->nama_jabatan}}</td>
                                        <td class="text-center">
                                            <form action="/{{$data->id}}/ubah_aktif_pegawai" method="POST">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="PUT">
                                            @if($data->aktif=="Y")
                                                <input type="submit" class="btn btn-success btn-sm" onclick="return confirm('Yakin Mau Mengubah {{$data->nama}} Menjadi Tidak Aktif?')" value="Aktif" name="status">
                                            @else
                                               <input type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Mengubah {{$data->nama}} Menjadi Aktif?')" value="Tidak Aktif" name="status">
                                            @endif
                                          </form>
                                        </td>
                                        <td class="text-center">
                                            <form method="POST" action="/{{$data->id}}/delete_pegawai">
                                            {{csrf_field()}}
                                        	   <a href="{{ url('/') }}/{{$data->id}}/detail_pegawai" class="btn btn-info btn-sm"><i class="fa fa-external-link"></i> Detail</a>
                                        	   <a href="/{{$data->id}}/edit_pegawai" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                        	   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau hapus data {{ $data->nama }}?')" name="delete"><i class="fa fa-eraser"></i> Hapus</button>
                                               <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
								@else
									<tr>
										<td colspan="5"><h1 align="center">Data Tidak Ditemukan</h1></td>				
										</td>
									</tr>
                    			@endif
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@endsection
