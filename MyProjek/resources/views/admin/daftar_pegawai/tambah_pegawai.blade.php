
@extends('admin.layout')

@section('title')
    Tambah Pegawai
@endsection

@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Tambah Pegawai</h1>
	    </div>
    </div>
    <div class="row">
    	<div class="col-md-12" style="margin-bottom:10px">
             <a href="{{ url('/daftar_pegawai') }}" class="btn btn-danger" onclick="window.history.go(-1)"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
            <form action="/p_tambah" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="col-md-8">
                @if ($errors->any())
                    <div class="alert alert-danger alert-message">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <table class="table table-stripped">
                    <tr>
                        <td style="width:30%"><label>NIK :</label></td>
                        <td><input type="number" class="form-control" name="nik" placeholder="Masukkan Nik" required></td>
                    </tr>
                    <tr>
                        <td><label>Nama Pegawai :</label></td>
                        <td><input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" required> </td>
                    </tr>
                    <tr>
                        <td><label>Jenis Kelamin :</label></td>
                        <td><input type="radio" name="jk" value="L" required>Laki-Laki 
                            <input type="radio" name="jk" value="P" required>Perempuan</td>
                    </tr>
                    <tr>
                        <td><label> Divisi :</label></td>
                        <td>
                            <select name="divisi" id="divisi" class="form-control" required>

                                <option value="">-- Pilih Divisi --</option>
                                @foreach($divisi as $data)
                                    <option value="{{$data->id}}">{{$data->nama_divisi}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Jabatan :</label></td>
                        <td>
                            <select name="jabatan" id="jabatan" class="form-control" required>

                                <option value="">-- Pilih Jabatan --</option>
                                @foreach($jabatan as $data2)
                                    <option id="jabatan" class="{{$data2->id_divisi}}" value="{{$data2->id}}">{{$data2->nama_jabatan}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Email:</label></td>
                        <td><input type="email" class="form-control" name="email" placeholder="Masukkan Email" required> </td>
                    </tr>
                    <tr>
                        <td><label> Aktif :</label></td>
                        <td><input type="radio" name="status" value="Y" required>Ya <input type="radio" name="status" value="T" required>Tidak</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
             </div> 
             <div class="col-md-4">
            <div class="control-group increment">
                <label class="control-label">Foto </label><br/>
                <img src="{{ asset('image/default-profile.png') }}" id="showgambar" style="width:150px;margin:5px" />
                <div class="input-group" >
                  <input type="file" name="foto" style="padding-bottom:30px !important;padding-top:2px" id="inputgambar" class="form-control input-sm">
                </div>
            </div>
             </div> 
             <div class="col-md-12">
                 <input type="submit" class="btn btn-primary" value="Simpan"> 
                <input type="reset" class="btn btn-warning" value="Reset"> 
             </div>
            </form>
        
                <!-- /.col-lg-12 -->
    </div>

    <script>
            $("#jabatan").chained("#divisi");
        </script>

    <!-- /.row -->
    <script type="text/javascript">

      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar").change(function () {
        readURL(this);
    });

</script>
@endsection
