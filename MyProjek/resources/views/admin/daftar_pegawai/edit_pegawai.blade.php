
@extends('admin.layout')

@section('title')
    Edit Pegawai
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Edit Pegawai</h1>
	    </div>
    </div>
    <div class="row">
    	<div class="col-md-12" style="margin-bottom:10px">
             <a href="{{ url('/daftar_pegawai') }}" class="btn btn-danger" onclick="window.history.go(-1)"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
            <form action="/{{$data->id}}/p_edit" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="PUT">
            <div class="col-md-8">
                @if ($errors->any())
                    <div class="alert alert-danger alert-message">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <table class="table table-stripped">
                    <tr>
                        <td style="width:30%"><label>NIK :</label></td>
                        <td><input type="number" class="form-control" name="nik"  value="{{$data->nik}}" required> </td>
                    </tr>
                    <tr>
                        <td><label>Nama Pegawai :</label></td>
                        <td><input type="text" class="form-control" name="nama"  value="{{$data->nama}}" required> </td>
                    </tr>
                    <tr>
                        <td><label>Jenis Kelamin :</label></td>
                        <td>
                            @if($data->jenis_kelamin=="L")
                                <input type="radio" name="jk" value="L" checked required>Laki-Laki 
                                <input type="radio" name="jk" value="P" required>Perempuan
                            @else
                                 <input type="radio" name="jk" value="L" required>Laki-Laki 
                                <input type="radio" name="jk" value="P" checked required>Perempuan
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><label> Divisi :</label></td>
                        <td>
                            <select name="divisi" id="divisi" class="form-control">
                                <option value="">-- Pilih Divisi --</option>
                                @foreach($divisi as $data2)
                                    <option value="{{$data2->id}}" 
                                        <?php 
                                         if ($data->id_divisi==$data2->id): 
                                           echo "selected"; 
                                         else:    
                                         endif ?>
                                    >{{$data2->nama_divisi}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Jabatan :</label></td>
                        <td>
                            <select name="jabatan" id="jabatan" class="form-control" required>

                                <option value="">-- Pilih Jabatan --</option>
                                @foreach($jabatan as $data3)
                                    <option id="jabatan" class="{{$data3->id_divisi}}" value="{{$data3->id}}"
                                        <?php 
                                         if ($data->id_jabatan==$data3->id): 
                                           echo "selected"; 
                                         else:    
                                         endif ?>
                                         >{{$data3->nama_jabatan}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
             </div> 
             <div class="col-md-4">
            <div class="control-group increment">
                <label class="control-label">Foto </label><br/>
                <img src="{{ asset('image/'.$data->gambar) }}" id="showgambar" style="width:150px;margin:5px" />
                <input type="hidden" value="{{$data->gambar}}" name="nama_gambar">
                <div class="input-group" >
                  <input type="file" name="foto" style="padding-bottom:30px !important;padding-top:2px" id="inputgambar" class="form-control input-sm">
                </div>
            </div>
             </div> 
             <div class="col-md-12">
                 <input type="submit" class="btn btn-primary" value="Simpan"> 
                <input type="reset" class="btn btn-warning" value="Reset"> 
             </div>
            </form>
        
                <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <script>
        $("#jabatan").chained("#divisi");
    </script>

    <script type="text/javascript">

      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar").change(function () {
        readURL(this);
    });

</script>
@endsection
