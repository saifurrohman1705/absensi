
@extends('admin.layout')

@section('title')
    Daftar Divisi
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Daftar Divisi</h1>
	    </div>
    </div>
    <div class="row">
    			<div class="col-md-12" style="margin-bottom:10px">
                    <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalTambah"><i class="fa fa-plus"></i> Tambah</button>
                    <!-- Modal -->
                    <div id="ModalTambah" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                        <form action="p_tambah_divisi" method="POST">
                        {{csrf_field()}}
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah Divisi</h4>
                          </div>
                          <div class="modal-body">
                            <input type="text" class="form-control" name="nama_divisi" placeholder="Masukkan Nama Divisi" required>
                          </div>
                          <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" value="Tambah"> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="col-lg-12">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b>Tabel Divisi</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">	
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width:20px">#</th>
                                        <th>Nama Divisi</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($divisi))
									@foreach($divisi as $data)
                                    <tr class="odd gradeX">
                                        <td>{{$no++}}</td>
                                        <td>{{$data->nama_divisi}}</td>
                                        <td class="text-center">
                                            <form method="POST" action="/{{$data->id}}/delete_divisi">
                                            {{csrf_field()}}
                                        	   <a href="{{url('/')}}/{{$data->id}}/daftar_jabatan" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Lihat Jabatan</a>
                                        	   <button type="button" class=" btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalEdit{{$data->id}}"><i class="fa fa-external-link"></i> Edit</button>
                                        	   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau hapus data {{ $data->nama_divisi }}?')" name="delete"><i class="fa fa-eraser"></i> Hapus</button>
                                               <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                        </td>
                                    </tr>
                                    
                                    {{-- Modal Edit --}}
                                    <div id="ModalEdit{{$data->id}}" class="modal fade" role="dialog">
                                          <div class="modal-dialog modal-sm">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                            <form action="/{{$data->id}}/p_edit_divisi" method="POST">
                                            {{csrf_field()}}
                                             <input type="hidden" name="_method" value="PUT">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Edit <span style="font-size:12px" class="label label-success">{{$data->nama_divisi}}</span></h4>
                                              </div>
                                              <div class="modal-body">
                                                <input type="text" class="form-control" name="nama_divisi" value="{{$data->nama_divisi}}" required>
                                              </div>
                                              <div class="modal-footer">
                                                <input type="submit" class="btn btn-primary" value="Simpan"> 
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                              </form>
                                            </div>

                                          </div>
                                    </div>
                                    @endforeach
								@else
									<tr>
										<td colspan="5"><h1 align="center">Data Tidak Ditemukan</h1></td>				
										</td>
									</tr>
                    			@endif
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection
