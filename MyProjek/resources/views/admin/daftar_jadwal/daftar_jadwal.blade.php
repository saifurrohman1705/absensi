
@extends('admin.layout')

@section('title')
    Daftar Jadwal
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Daftar Jadwal</h1>
	    </div>
    </div>
    <div class="row">
    			<div class="col-md-12" style="margin-bottom:10px">
                    <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalTambah"><i class="fa fa-plus"></i> Tambah</button>

                    <!-- Modal -->
                    <div id="ModalTambah" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-md">

                        <!-- Modal content-->
                        <div class="modal-content">
                        <form action="{{ url('/p_tambah_jadwal') }}" method="POST">
                        {{csrf_field()}}
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah Jadwal</h4>
                          </div>
                            <style>
                               .control-label{
                                  margin-top:10px !important;
                               };
                            </style>
                            <style>
                              .ket_input{margin-left:5px;font-weight:bold;color:grey;font-size:12px}
                            </style>
                          <div class="modal-body">
                            <label class="control-label">Hari:</label>
                            <select name="hari" class="form-control" required>
                              <?php $hari=array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu') ?>
                                @foreach($hari as $day)
                                  <option>{{$day}}</option>
                                @endforeach
                            </select>

                            <label class="control-label">Jadwal:</label>
                            <input type="text" class="form-control" name="jadwal" placeholder="exp: regular, libur, meeting" required>

                            <label class="control-label">Jam Masuk:</label>
                            <input type="time" class="form-control" name="masuk" >

                            <label class="control-label">Jam Pulang:</label>
                            <input type="time" class="form-control" name="keluar" >

                            <label class="control-label">Toleransi Keterlambatan:</label>
                            <input type="number" class="form-control" name="toleransi_masuk">
                            <p class="ket_input">Menit Setelah Jam Masuk</p>

                            <label class="control-label">Toleransi Maksimal Waktu Pulang:</label>
                            <input type="number" class="form-control" name="toleransi_pulang">
                            <p class="ket_input">Menit Setelah Jam Pulang</p>

                          </div>
                          <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" value="Tambah"> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>

                      </div>
                    </div>
                </div>
                <div class="col-lg-12">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b>Tabel Jadwal</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">	
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width:20px">#</th>
                                        <th>Hari</th>
                                        <th>Jadwal</th>
                                        <th>Masuk</th>
                                        <th>Keluar</th>
                                         <th>Tol Keterlambatan</th>
                                        <th class="text-center">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($jadwal))
									             @foreach($jadwal as $data)
                                    <tr class="odd gradeX">
                                        <td>{{$no++}}</td>
                                        <td>{{$data->hari}}</td>
                                        <td>{{$data->jadwal}}</td>
                                        <td>{{$data->masuk}}</td>
                                        <td>{{$data->keluar}}</td>
                                        <td align="center">{{$data->toleransi_masuk}} Menit</td>
                                        <td class="text-center">
                                            <form method="POST" action="{{url('/')}}/{{$data->id}}/delete_jadwal">
                                            {{csrf_field()}}
                                        	   <button type="button" class=" btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalEdit{{$data->id}}"><i class="fa fa-pencil-square-o"></i> Edit</button>
                                        	   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau hapus data {{ $data->hari }}?')" name="delete"><i class="fa fa-eraser"></i> Hapus</button>
                                               <input type="hidden" name="_method" value="DELETE">
                                            </form>
                                        </td>
                                    </tr>
                                    
                                    {{-- Modal Edit --}}
                                    <div id="ModalEdit{{$data->id}}" class="modal fade" role="dialog">
                                          <div class="modal-dialog modal-md">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                            <form action="{{url('/')}}/{{$data->id}}/p_edit_jadwal" method="POST">
                                            {{csrf_field()}}
                                             <input type="hidden" name="_method" value="PUT">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Edit Hari <span style="font-size:12px" class="label label-success">{{$data->hari}}</span></h4>
                                              </div>
                                              <div class="modal-body">
                                                <label class="control-label">Hari:</label>
                                                <select name="hari" class="form-control" required>
                                                <?php $hari=array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu') ?>
                                                @foreach($hari as $day)
                                                  <option
                                                    <?php 
                                                     if ($day==$data->hari): 
                                                       echo "selected"; 
                                                     else:    
                                                     endif ?>
                                                  >{{$day}}</option>
                                                @endforeach
                                                </select>
                                                <label class="control-label">Jadwal:</label>
                                                <input type="text" class="form-control" name="jadwal" value="{{$data->jadwal}}" placeholder="exp: regular, libur, meeting" required>

                                                <label class="control-label">Jam Masuk:</label>
                                                <input type="time" class="form-control" name="masuk" value="{{$data->masuk}}" >

                                                <label class="control-label">Jam Keluar:</label>
                                                <input type="time" class="form-control" name="keluar" value="{{$data->keluar}}" >

                                                <label class="control-label">Toleransi Keterlambatan:</label>
                                                <input type="number" class="form-control" name="toleransi_masuk" value="{{$data->toleransi_masuk}}">
                                                <p class="ket_input">Menit Setelah Jam Masuk</p>

                                                <label class="control-label">Toleransi Maksimal Waktu Pulang:</label>
                                                <input type="number" class="form-control" name="toleransi_pulang" value="{{$data->toleransi_pulang}}">
                                                <p class="ket_input">Menit Setelah Jam Pulang</p>
                                              </div>
                                              <div class="modal-footer">
                                                <input type="submit" class="btn btn-primary" value="Simpan"> 
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              </div>
                                              </form>
                                            </div>

                                          </div>
                                    </div>
                                    @endforeach
								@else
									<tr>
										<td colspan="5"><h1 align="center">Data Tidak Ditemukan</h1></td>				
										</td>
									</tr>
                    			@endif
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

@endsection
