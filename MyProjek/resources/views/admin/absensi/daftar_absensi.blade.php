
@extends('admin.layout')

@section('title')
    Daftar Absensi
@endsection



@section('content')

	<div class="row">
	    <div class="col-lg-12">
	        <h1 class="page-header">Daftar Absensi</h1>
	    </div>
    </div>
    <div class="row">
    			<div class="col-md-12" style="margin-bottom:10px">
                    <a href="{{ url('/tambah_pegawai') }}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                <div class="col-lg-12">

                    <div class="panel panel-info">
                        <div class="panel-heading text-center">
                            <b>Jadwal Hari Ini: {{$jadwal->hari." (".$jadwal->masuk." - ".$jadwal->keluar.")"}}</b>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	
                        	
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="width:20px">#</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Masuk</th>
                                        <th class="text-center">Keluar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <tr class="odd gradeX">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center"></td>
                                      
                                    </tr>
                                  
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
@endsection
