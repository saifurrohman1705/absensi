<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    protected $table = "tb_divisi";
    protected $fillable = ['nama_divisi'];
}
