<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = "tb_pegawai";
    protected $fillable = ['nik','nama','gambar','jenis_kelamin','id_divisi','id_jabatan','aktif','email','id_users'];
    
}
