<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;
use App\Divisi;

class JabatanController extends Controller
{
    public function index($id)
    {
    	$jabatan = Jabatan::where('id_divisi',$id)->orderby('id','desc')->get(); 
    	$divisi = Divisi::select('id','nama_divisi')->where('id',$id)->first();       
        $no=1;
		return view('admin.master_divisi.daftar_jabatan', compact('jabatan','divisi'))
		->with('no', $no);
    }

    public function insert_data(Request $data)
    {
    	$id=$data->id_divisi;
        $link= "/".$id."/daftar_jabatan";
    	$db = new Jabatan;
    	$db->nama_jabatan 	= ucwords($data->nama_jabatan);
    	$db->id_divisi		= $id;

    	$db->save(); // query insert
        echo "<script>alert('Data Berhasil Ditambahkan!');
         document.location.href='$link'</script>";
    	
    }

    function update_data(Request $data, $id)
    {
    	$id_divisi=$data->id_divisi;
        $link= "/".$id_divisi."/daftar_jabatan";
    	$db = Jabatan::find($id);
    	$db->nama_jabatan 	= ucwords($data->nama_jabatan);
    	$db->id_divisi		= $id_divisi;

    	$db->save(); // query update

        echo "<script>alert('Data Berhasil Diedit!');
         document.location.href='$link'</script>";
    }

    function delete_data(Request $data, $id)
    {
    	$id_divisi=$data->id_divisi;
        $link= "/".$id_divisi."/daftar_jabatan";
        $db = Jabatan::find($id); 
        $db->delete();
        echo "<script>alert('Data Berhasil Dihapus!');
         document.location.href='$link'</script>";
    }
}
