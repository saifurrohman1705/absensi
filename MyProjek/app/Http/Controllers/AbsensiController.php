<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Jadwal;

class AbsensiController extends Controller
{
    public function index()
    {	
    	$day = date('D');
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);

		$pegawai = pegawai::orderby('id','desc')->get();   
		$jadwal = Jadwal::Where('hari',$dayList[$day])->first();
		$no=1;
		return view('admin.absensi.daftar_absensi', compact('jadwal','pegawai'))
		->with('no', $no);
	}

	
}
