<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pegawai;
use App\Divisi;
use App\User;
use App\Jabatan;

class PegawaiController extends Controller
{
    public function index()
    {
        
    	$pegawai =pegawai::select('tb_pegawai.*','tb_jabatan.nama_jabatan')->join('tb_jabatan', 'tb_pegawai.id_jabatan', '=', 'tb_jabatan.id')->orderby('tb_pegawai.id','desc')->get();        
        $no=1;
		return view('admin.daftar_pegawai.daftar_pegawai', compact('pegawai'))
		->with('no', $no);
    }

    public function tambah()
    {
        $divisi = Divisi::orderby('id','desc')->get();
		$jabatan = Jabatan::select('tb_jabatan.id','tb_jabatan.nama_jabatan','tb_jabatan.id_divisi')->join('tb_divisi', 'tb_jabatan.id_divisi', '=', 'tb_divisi.id')->orderby('tb_jabatan.id','desc')->get();
    	return view('admin.daftar_pegawai.tambah_pegawai', compact('divisi','jabatan'));

    }

    public function insert_data(Request $data)
    {
    		$this->validate($data, [
		    'nik' => 'unique:tb_pegawai|max:16|min:16',
            'email' => 'unique:tb_pegawai',  

		]);
         
            //insert tb_pegawai
    	    $pegawai = new pegawai;
    		$pegawai->nama            = $data->nama;
    		$pegawai->nik             = $data->nik;
    		$pegawai->jenis_kelamin   = $data->jk;
    		$pegawai->id_jabatan      = $data->jabatan;
            $pegawai->id_divisi       = $data->divisi;
    		$pegawai->aktif           = $data->status;
            $pegawai->email           = $data->email;

    			 if($data->file('foto') == "")
           		 {
               	 }else{
	                // Disini proses mendapatkan judul dan memindahkan letak gambar ke folder image
	                $file       = $data->file('foto');
	                $fileName   = $file->getClientOriginalName();
	                $data->file('foto')->move("image/", $fileName);

	                $pegawai->gambar = $fileName;
               	}

    	$pegawai->save(); // query insert

        //insert users
        // $users = new user;
            // $users->username        = $data->nik;
            // $users->password        = bcrypt('datakreatif');
        // $isOk  = password_verify('hash code', $user);
        // if ($isOk) 
        // {
            // echo "yes";# code...
        // }
        // else
            // echo "no";

    	echo "<script>alert('Data Berhasil Ditambahkan!');
         document.location.href='/daftar_pegawai'</script>";
    }

     public function detail_data($id)
    {
        $pegawai = pegawai::where('tb_pegawai.id', $id)->join('tb_divisi', 'tb_pegawai.id_divisi', '=', 'tb_divisi.id')->join('tb_jabatan', 'tb_pegawai.id_jabatan', '=', 'tb_jabatan.id')->first(); // atau $pegawai = pegawai::find($id); 
        return view('admin.daftar_pegawai.detail_pegawai')
        ->with('data', $pegawai);
    }

    function input_edit($id)
    {
        $pegawai = pegawai::where('id', $id)->first(); // atau $pegawai = pegawai::find($id); 

        // mengecek apakah tabel ada isinya menggunakan syntax: dd($pegawai);
        $divisi = Divisi::orderby('id','desc')->get();
        $jabatan = Jabatan::select('tb_jabatan.id','tb_jabatan.nama_jabatan','tb_jabatan.id_divisi')->join('tb_divisi', 'tb_jabatan.id_divisi', '=', 'tb_divisi.id')->orderby('tb_jabatan.id','desc')->get();
        return view('admin.daftar_pegawai.edit_pegawai',compact('divisi','jabatan'))
        ->with('data', $pegawai);
    }

    function update_data(Request $data, $id)
    {
        $this->validate($data, [
            'nik' => 'max:16|min:16', 

        ]);
        $pegawai = pegawai::find($id);
            $pegawai->nama            = $data->nama;
            $pegawai->nik             = $data->nik;
            $pegawai->jenis_kelamin   = $data->jk;
            $pegawai->id_jabatan      = $data->jabatan;
            $pegawai->id_divisi       = $data->divisi;

            if($data->file('foto') == "")
            {
                $pegawai->gambar = $pegawai->gambar;
            } 
            else
            {
                //menghapus gambar lama
                $file_path = public_path().'/image/'.$pegawai->gambar;
                if($data->nama_gambar!="default-profile.png"){
                    unlink($file_path);
                }else{}
                // Disini proses mendapatkan judul dan memindahkan letak gambar ke folder image
                $file       = $data->file('foto');
                $fileName   = $file->getClientOriginalName();
                $data->file('foto')->move("image/", $fileName);

                $pegawai->gambar = $fileName;
            }

        $pegawai->save(); // query update

        //mengupdate username pada tabel users apabila nik dirubah
        $db = user::where('id_pegawai',$id)->first(); 
            $db->username = $data->nik; 
            $db->nama     = $data->nama;
        $db->save();


        echo "<script>alert('Data Berhasil Di edit!');
         document.location.href='/daftar_pegawai'</script>";
    }

    function update_aktif(Request $data, $id){
        if ($data->status=="Aktif") {
            $status="T";
        }else{
            $status="Y";
        }
        $pegawai = pegawai::where('id', $id)->first();
            $pegawai->aktif = $status;
        $pegawai->save();

        $user    = user::where('id_pegawai',$id)->first();
        if(Count($user)>0){
            $user->aktif    = $status;
        $user->save();
        }
        echo "<script>alert('Data Berhasil Di edit!');
         document.location.href='/daftar_pegawai'</script>";
    }

    function delete_data($id)
    {
        $pegawai = pegawai::find($id); // Select * from pegawai where primary key = $id
        // jika yang dihapus berdasarkan nama: $pegawai = pegawai::where('nama,'=',$id)
        $def_img    = $pegawai->gambar;
        $filename = $pegawai->gambar;
       
        $file_path = public_path().'/image/'.$filename;
        if($def_img!="default-profile.png"){
                    unlink($file_path);
                }else{}
        $pegawai->delete();
        //delete in tb_users where id_pegawai = $id
        $db = user::where('id_pegawai',$id); 
        $db->delete();



        echo "<script>alert('Data Berhasil Dihapus!');
         document.location.href='/daftar_pegawai'</script>";
    }

    

}
