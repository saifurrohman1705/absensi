<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Pegawai;
use App\Jabatan;

class UsersController extends Controller
{
    public function index()
    {       
        $user = User::where('level','!=','0')->orderby('id','desc')->get();        
        $no=1;
		return view('admin.daftar_user.daftar_user',compact('user'))
		->with('no', $no);
    }

    public function user($id)
    {       
        $pegawai =pegawai::select('tb_pegawai.*','tb_jabatan.nama_jabatan')->join('tb_jabatan', 'tb_pegawai.id_jabatan', '=', 'tb_jabatan.id')->orderby('tb_pegawai.id','desc')->get();        
        $no=1;
        if ($id==1) {
        	$nama="Operator";
        }
        else{
        	$nama="User";
        }
		return view('admin.daftar_user.tambah_user', compact('pegawai','no','nama'))
		->with('id', $id);
    }

    public function insert_data(Request $data)
    {
        $cek_id     =user::where('id_pegawai',$data->id_pegawai)->first();  
        if(Count($cek_id)>0){

            $cek_lvl_asc  =user::select('level')->where('id_pegawai',$data->id_pegawai)->orderby('level','ASC')->first();
            $cek_lvl_desc  =user::select('level')->where('id_pegawai',$data->id_pegawai)->orderby('level','DESC')->first();
            if($cek_lvl_asc->level==$data->level OR $cek_lvl_desc->level==$data->level){
                echo "<script>alert('Data Sudah Ada!');
                document.location.href='/daftar_user'</script>";
            }else{
                $db = new User;
                $db->nama = $data->nama;
                $db->username = $data->username;
                $db->id_pegawai = $data->id_pegawai;
                $db->level = $data->level;
                $db->password = bcrypt('datakreatif');

                $db->save(); // query insert

                 echo "<script>alert('Data Berhasil Ditambahkan!');
                    document.location.href='/daftar_user'</script>";
            }
        }else{
            $db = new User;
                $db->nama = $data->nama;
                $db->username = $data->username;
                $db->id_pegawai = $data->id_pegawai;
                $db->level = $data->level;
                $db->password = bcrypt('datakreatif');

                $db->save(); // query insert

                 echo "<script>alert('Data Berhasil Ditambahkan!');
                    document.location.href='/daftar_user'</script>";
        }
    }

	function update_data(Request $data, $id)
    {
    	if ($data->status=="Aktif") {
    		$status="T";
    	}else{
    		$status="Y";
    	}
        $db = user::where('id',$id)->first();
        $db->aktif = $status;
        $db->save(); // query update

        //mengubah status aktif pada tb_pegawai
        $id_pegawai = $db->id_pegawai;
        $pegawai = pegawai::where('id',$id_pegawai)->first();
        $pegawai->aktif = $status;
        $pegawai->save(); // query update

        echo "<script>alert('Data Berhasil Diubah!');
         document.location.href='/daftar_user'</script>";  
    }

    function delete_data($id)
    {
        $db = user::find($id); 
        $db->delete();
        echo "<script>alert('Data Berhasil Dihapus!');
         document.location.href='/daftar_user'</script>";
    }




}
