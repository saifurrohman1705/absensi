<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;
use App\MasterDivisi;
class JadwalController extends Controller
{
    public function index()
    {
    	$jadwal = Jadwal::orderby('id','asc')->get();        
        $no=1;
		return view('admin.daftar_jadwal.daftar_jadwal', compact('jadwal'))
		->with('no', $no);
    }

    public function insert_data(Request $data)
    {
    	$db = new Jadwal;
    	$db->hari = $data->hari;
        $db->jadwal = ucwords($data->jadwal);
    	$db->masuk = $data->masuk;
    	$db->keluar = $data->keluar;
        $db->toleransi_masuk = $data->toleransi_masuk;
        $db->toleransi_pulang = $data->toleransi_pulang;

    	$db->save(); // query insert

        echo "<script>alert('Data Berhasil Ditambahkan!');
         document.location.href='/daftar_jadwal'</script>";
    }

    function update_data(Request $data, $id)
    {

    	$db = Jadwal::where('id',$id)->first();
	    	$db->hari = $data->hari;
            $db->jadwal = ucwords($data->jadwal);
	    	$db->masuk = $data->masuk;
	    	$db->keluar = $data->keluar;
            $db->toleransi_masuk = $data->toleransi_masuk;
            $db->toleransi_pulang = $data->toleransi_pulang;
    	$db->save(); // query update
        echo "<script>alert('Data Berhasil Diedit!');
         document.location.href='/daftar_jadwal'</script>";
    }

    function delete_data($id)
    {
        $db = Jadwal::find($id); 
        $db->delete();
        echo "<script>alert('Data Berhasil Dihapus!');
         document.location.href='/daftar_jadwal'</script>";
    }
}
