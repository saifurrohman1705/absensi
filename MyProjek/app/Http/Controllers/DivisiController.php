<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Divisi;

class DivisiController extends Controller
{
    public function index()
    {
    	$divisi = Divisi::orderby('id','desc')->get();        
        $no=1;
		return view('admin.master_divisi.daftar_divisi', compact('divisi'))
		->with('no', $no);
    }

    public function insert_data(Request $data)
    {
    	$db = new Divisi;
    	$db->nama_divisi = ucwords($data->nama_divisi);

    	$db->save(); // query insert

        echo "<script>alert('Data Berhasil Ditambahkan!');
         document.location.href='/daftar_divisi'</script>";
    }

    function update_data(Request $data, $id)
    {

    	$db = Divisi::find($id);
    		$db->nama_divisi = ucwords($data->nama_divisi);
    	$db->save(); // query update

    	echo "<script>alert('Data Berhasil Diedit!');
         document.location.href='/daftar_divisi'</script>";
    }

    function delete_data($id)
    {
        $db = Divisi::find($id); 
        $db->delete();
        echo "<script>alert('Data Berhasil Dihapus!');
         document.location.href='/daftar_divisi'</script>";
    }
}
