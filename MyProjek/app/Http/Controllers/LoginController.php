<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    
    function cek_login(Request $data)
    {
    	$this->validate($data, [
		    'username' => 'required',
            'password' => 'required',  

		]);

		$username=$data->username;
    	$tampil=user::where('username',$username)->first();
    	if(Count($tampil)>0){
    		if($tampil->aktif=="Y"){
		    	if(password_verify ( $data->password , $tampil->password )){
		    		if($data->password!="datakreatif"){
			    		$id_user	= $tampil->id;
			    		$nama_user	= $tampil->nama;
			    		$level		= $tampil->level;

		                Session::put('id_user', $id_user);
		                Session::put('level', $level);
		                Session::put('login', TRUE);

			    		if($level==0){
				    		echo "<script>alert('Selamat Datang $tampil->nama! Anda Akan Menuju Halaman Dashboard...');
				         	document.location.href='/dashboard'</script>";
				         }else if($level==1){
				         	echo "Kamu Operator";
				         }else{
				         	echo "<script>alert('Selamat Datang $tampil->nama! Anda Akan Menuju Halaman Dashboard...');
				         	document.location.href='/user/dashboard'</script>";
				         }
			     	}else{
			     		$id_user	= $tampil->id;
			     		Session::put('id', encrypt($id_user));
			     		return redirect('/change_password')->with('alert-sukses','Selamat! Sekarang Buat Password Baru Anda..');
			     	}
		        }else{
		        	return redirect('/')->with('alert','Password Salah! Silahkan Login Kembali..');
		        }
		     }else{
		     	return redirect('/')->with('alert','Akun $tampil->nama Tidak Aktif! Hubungi Administrator..');
		     }
	    }else{

         	return redirect('/')->with('alert','Username Tidak Terdaftar! Silahkan Login Kembali..');
	    }

    }

    public function logout(){
        Session::flush();
         return redirect('/')->with('alert','Kamu sudah logout');
    }

    function change_password(Request $data){

    	$this->validate($data, [
            'NewPassword' => 'required',  
            'ConfirmPassword' => 'required',

		]);

    	if($data->password==$data->c_password){
	    	$dcr_id	=decrypt($data->enc_id);
	    	$db 		= user::find($dcr_id);
		    	$db->password = bcrypt($data->NewPassword);
	    	$db->save(); // query update
	    	return redirect('/')->with('alert-sukses','Anda Berhasil Memperbarui Password, Silahkan Login Dengan Password Baru Anda!');
	    }else{
	    	return redirect('/change_password')->with('alert-gagal','Password Tidak Sama, Coba Lagi!');
	    }
    }
}
