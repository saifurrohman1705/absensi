<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = "tb_jabatan";
    protected $fillable = ['nama_jabatan','id_divisi'];
}
