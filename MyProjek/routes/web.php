<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login
Route::get('/', function () {
    return view('login.login');
});

//cek login
Route::post('/cek_login', 'LoginController@cek_login');

//change password at first time
Route::get('/change_password', function () {
	return view('login.change_password');
});

Route::put('/p_new_pass', 'LoginController@change_password');


//Halaman User
//=================================================================================//
//mengarahkan ke dashboard
Route::get('/dashboard', function () {
	return view('admin.dashboard');
});

// Logout
Route::get('/logout', 'LoginController@logout');

//Menu
Route::get('/user/dashboard', 'user\DashboardController@index');
Route::get('/user/absensi', 'user\AbsenController@index');


//=================================================================================//

// Menu Pegawai
Route::get('/daftar_pegawai', 'PegawaiController@index');
Route::get('/tambah_pegawai', 'PegawaiController@tambah');
Route::post('/p_tambah', 'PegawaiController@insert_data');
Route::get('/{id}/detail_pegawai/', 'PegawaiController@detail_data');
Route::get('/{id}/edit_pegawai/', 'PegawaiController@input_edit');
Route::put('/{id}/p_edit', 'PegawaiController@update_data');
Route::delete('/{id}/delete_pegawai/', 'PegawaiController@delete_data');
Route::put('/{id}/ubah_aktif_pegawai/', 'PegawaiController@update_aktif');

//Menu Divisi
Route::get('/daftar_divisi', 'DivisiController@index');
Route::post('/p_tambah_divisi', 'DivisiController@insert_data');
Route::put('/{id}/p_edit_divisi', 'DivisiController@update_data');
Route::delete('/{id}/delete_divisi/', 'DivisiController@delete_data');

//Halaman lihat jabatan
Route::get('/{id}/daftar_jabatan', 'JabatanController@index');
Route::post('/p_tambah_jabatan', 'JabatanController@insert_data');
Route::put('/{id}/p_edit_jabatan', 'JabatanController@update_data');
Route::delete('/{id}/delete_jabatan/', 'JabatanController@delete_data');

//Menu Jadwal
Route::get('/daftar_jadwal', 'JadwalController@index');
Route::post('/p_tambah_jadwal', 'JadwalController@insert_data');
Route::put('/{id}/p_edit_jadwal', 'JadwalController@update_data');
Route::delete('/{id}/delete_jadwal/', 'JadwalController@delete_data');

//Menu Absensi
Route::get('/daftar_absensi', 'AbsensiController@index');

//Menu Tambah User
Route::get('/daftar_user', 'UsersController@index');
Route::get('/{id}/tambah_user', 'UsersController@user');
Route::post('/p_tambah_user', 'UsersController@insert_data');
Route::put('/{id}/ubah_aktif_user', 'UsersController@update_data');
Route::delete('/{id}/delete_users/', 'UsersController@delete_data');

//Izin
Route::get('/daftar_izin', 'IzinController@index');
Route::get('/tambah_izin', 'IzinController@tambah');
Route::post('/p_tambah_izin', 'IzinController@insert_data');
Route::put('/{id}/p_edit_izin', 'IzinController@update_data');
Route::delete('/{id}/delete_izin/', 'IzinController@delete_data');






