<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pegawai', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->integer('nik')->unique();
            $table->string('nama', 35);
            $table->string('gambar');
            $table->integer('id_divisi');
            $table->integer('id_jabatan');
            $table->enum('aktif', ['Y', 'T']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('tb_pegawai');
    }
}
